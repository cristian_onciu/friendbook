package client;

import java.rmi.RemoteException;

/**
 * @author Cristian Onciu
 *
 */
public class ClientTest
	{
		public static void main(String[] args)
			{
				ClientController controller;
				try
					{
						controller = new ClientController();
						ClientFrame frame = new ClientFrame(controller);
					} catch (RemoteException e)
					{
						e.printStackTrace();
					}
			}
	}
