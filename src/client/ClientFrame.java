package client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import model.User;

/**
 * @author Cristian Onciu
 *
 */
public class ClientFrame extends ClientView implements ActionListener
	{
		private ClientController	controller;
		private User							user;

		public ClientFrame(ClientController controller)
			{
				super("Rmi Client");
				this.controller = controller;
				this.btnExitActionListener(this);
				this.btnLoginActionListener(this);
				this.btnSendActionListener(this);
				controller.setFrame(this);
			}

		private void login()
			{
				if (getServerIP() != null && getUserName() != null
						&& !getUserName().equals(""))
					{
						if (!controller.checkUserExists(getServerIP(), getUserName()))
							{
								user = new User(this.getUserName());
								controller.login(getServerIP(), getUserName());
								SwingUtilities.invokeLater(new Runnable()
									{
										@Override
										public void run()
											{
												updateUserList(controller.updateUserList());
											}
									});
							} else
							JOptionPane.showMessageDialog(getContentPane(),
									"Please choose another nickname.",
									getUserName() + " exists!", JOptionPane.ERROR_MESSAGE);
					}
			}

		public void sendMessage()
			{
				if (getUserName() != null && getMessage() != null
						&& getSelectedUserFromList() != null)
					controller.sendMessage(getMessage(), getUserName(),
							getSelectedUserFromList());
			}

		@Override
		public void actionPerformed(ActionEvent arg0)
			{
				String action = arg0.getActionCommand();
				if (action.equals("Exit"))
					controller.logout();
				else if (action.equals("Login"))
					{
						login();
					} else if (action.equals("Send"))
					{
						sendMessage();
						clearMessageLine();
					}
			}
	}
