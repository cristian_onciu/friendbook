package client;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.util.Vector;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.StyledDocument;

/**
 * @author Cristian Onciu
 *
 */
public class ClientView extends JFrame
	{
		private JTextField messageField;
		private JTextArea messagesArea;
		private JList friendsList;
		private JButton btnLogin = new JButton("Login");
		private JButton btnAddFriend = new JButton("Add Friend");
		private JButton btnRemoveFriend = new JButton("Remove Friend");
		private JButton btnInviteUser = new JButton("Invite User");;
		private JButton btnExit = new JButton("Exit");
		private JButton btnSend = new JButton("Send");;
		private JList usersList;
		private JTextArea detailsArea;
		private JLabel lblServerIP;
		private JTextField userNameField;
		private JLabel lblUserName;
		private JTextField serverIPField;

		private JPanel createPanel()
			{
				JPanel contentPane = new JPanel();
				setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
				setBounds(100, 100, 450, 300);
				contentPane = new JPanel();
				contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
				setContentPane(contentPane);
				GridBagLayout gbl_contentPane = new GridBagLayout();
				gbl_contentPane.columnWidths = new int[] { 0, 0, 0, 0, 0 };
				gbl_contentPane.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
				gbl_contentPane.columnWeights = new double[] { 1.0, 1.0, 1.0, 0.0,
						Double.MIN_VALUE };
				gbl_contentPane.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0,
						0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
				contentPane.setLayout(gbl_contentPane);
				lblServerIP = new JLabel("Server IP:");
				GridBagConstraints gbc_lblServerIP = new GridBagConstraints();
				gbc_lblServerIP.insets = new Insets(0, 0, 5, 5);
				gbc_lblServerIP.gridx = 1;
				gbc_lblServerIP.gridy = 0;
				contentPane.add(lblServerIP, gbc_lblServerIP);
				lblUserName = new JLabel("User name:");
				GridBagConstraints gbc_lblUserName = new GridBagConstraints();
				gbc_lblUserName.insets = new Insets(0, 0, 5, 5);
				gbc_lblUserName.gridx = 2;
				gbc_lblUserName.gridy = 0;
				contentPane.add(lblUserName, gbc_lblUserName);
				serverIPField = new JTextField();
				serverIPField.setText("192.168.1.211");
				GridBagConstraints gbc_serverIPField = new GridBagConstraints();
				gbc_serverIPField.insets = new Insets(0, 0, 5, 5);
				gbc_serverIPField.fill = GridBagConstraints.HORIZONTAL;
				gbc_serverIPField.gridx = 1;
				gbc_serverIPField.gridy = 1;
				contentPane.add(serverIPField, gbc_serverIPField);
				serverIPField.setColumns(10);
				userNameField = new JTextField();
				GridBagConstraints gbc_userNameField = new GridBagConstraints();
				gbc_userNameField.insets = new Insets(0, 0, 5, 5);
				gbc_userNameField.fill = GridBagConstraints.HORIZONTAL;
				gbc_userNameField.gridx = 2;
				gbc_userNameField.gridy = 1;
				contentPane.add(userNameField, gbc_userNameField);
				userNameField.setColumns(10);
				btnLogin = new JButton("Login");
				GridBagConstraints gbc_btnLogin = new GridBagConstraints();
				gbc_btnLogin.fill = GridBagConstraints.HORIZONTAL;
				gbc_btnLogin.insets = new Insets(0, 0, 5, 0);
				gbc_btnLogin.gridx = 3;
				gbc_btnLogin.gridy = 1;
				contentPane.add(btnLogin, gbc_btnLogin);
				JScrollPane scrollPane = new JScrollPane();
				GridBagConstraints gbc_scrollPane = new GridBagConstraints();
				gbc_scrollPane.fill = GridBagConstraints.BOTH;
				gbc_scrollPane.gridheight = 4;
				gbc_scrollPane.gridwidth = 2;
				gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
				gbc_scrollPane.gridx = 0;
				gbc_scrollPane.gridy = 2;
				contentPane.add(scrollPane, gbc_scrollPane);
				messagesArea = new JTextArea();
				scrollPane.setViewportView(messagesArea);
				JScrollPane scrollPane_1 = new JScrollPane();
				GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
				gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
				gbc_scrollPane_1.gridheight = 4;
				gbc_scrollPane_1.insets = new Insets(0, 0, 5, 5);
				gbc_scrollPane_1.gridx = 2;
				gbc_scrollPane_1.gridy = 2;
				contentPane.add(scrollPane_1, gbc_scrollPane_1);
				friendsList = new JList();
				scrollPane_1.setViewportView(friendsList);
				GridBagConstraints gbc_btnAddFriend = new GridBagConstraints();
				gbc_btnAddFriend.fill = GridBagConstraints.HORIZONTAL;
				gbc_btnAddFriend.anchor = GridBagConstraints.NORTH;
				gbc_btnAddFriend.insets = new Insets(0, 0, 5, 0);
				gbc_btnAddFriend.gridx = 3;
				gbc_btnAddFriend.gridy = 2;
				contentPane.add(btnAddFriend, gbc_btnAddFriend);
				GridBagConstraints gbc_btnRemoveFriend = new GridBagConstraints();
				gbc_btnRemoveFriend.insets = new Insets(0, 0, 5, 0);
				gbc_btnRemoveFriend.gridx = 3;
				gbc_btnRemoveFriend.gridy = 3;
				contentPane.add(btnRemoveFriend, gbc_btnRemoveFriend);
				GridBagConstraints gbc_btnInviteUser = new GridBagConstraints();
				gbc_btnInviteUser.fill = GridBagConstraints.HORIZONTAL;
				gbc_btnInviteUser.insets = new Insets(0, 0, 5, 0);
				gbc_btnInviteUser.gridx = 3;
				gbc_btnInviteUser.gridy = 4;
				contentPane.add(btnInviteUser, gbc_btnInviteUser);
				GridBagConstraints gbc_btnExit = new GridBagConstraints();
				gbc_btnExit.insets = new Insets(0, 0, 5, 0);
				gbc_btnExit.fill = GridBagConstraints.HORIZONTAL;
				gbc_btnExit.gridx = 3;
				gbc_btnExit.gridy = 5;
				contentPane.add(btnExit, gbc_btnExit);
				JLabel lblMessage = new JLabel("Message:");
				GridBagConstraints gbc_lblMessage = new GridBagConstraints();
				gbc_lblMessage.insets = new Insets(0, 0, 5, 5);
				gbc_lblMessage.gridx = 0;
				gbc_lblMessage.gridy = 6;
				contentPane.add(lblMessage, gbc_lblMessage);
				messageField = new JTextField();
				GridBagConstraints gbc_messageField = new GridBagConstraints();
				gbc_messageField.gridwidth = 2;
				gbc_messageField.insets = new Insets(0, 0, 5, 5);
				gbc_messageField.fill = GridBagConstraints.HORIZONTAL;
				gbc_messageField.gridx = 1;
				gbc_messageField.gridy = 6;
				contentPane.add(messageField, gbc_messageField);
				messageField.setColumns(10);
				GridBagConstraints gbc_btnSend = new GridBagConstraints();
				gbc_btnSend.fill = GridBagConstraints.HORIZONTAL;
				gbc_btnSend.insets = new Insets(0, 0, 5, 0);
				gbc_btnSend.gridx = 3;
				gbc_btnSend.gridy = 6;
				contentPane.add(btnSend, gbc_btnSend);
				JLabel lblUsersOnline = new JLabel("Users online:");
				GridBagConstraints gbc_lblUsersOnline = new GridBagConstraints();
				gbc_lblUsersOnline.insets = new Insets(0, 0, 5, 5);
				gbc_lblUsersOnline.gridx = 0;
				gbc_lblUsersOnline.gridy = 7;
				contentPane.add(lblUsersOnline, gbc_lblUsersOnline);
				JLabel lblSelectAUser = new JLabel("Select a user for details:");
				GridBagConstraints gbc_lblSelectAUser = new GridBagConstraints();
				gbc_lblSelectAUser.insets = new Insets(0, 0, 5, 5);
				gbc_lblSelectAUser.gridx = 1;
				gbc_lblSelectAUser.gridy = 7;
				contentPane.add(lblSelectAUser, gbc_lblSelectAUser);
				JScrollPane scrollPane_2 = new JScrollPane();
				GridBagConstraints gbc_scrollPane_2 = new GridBagConstraints();
				gbc_scrollPane_2.fill = GridBagConstraints.BOTH;
				gbc_scrollPane_2.insets = new Insets(0, 0, 0, 5);
				gbc_scrollPane_2.gridx = 0;
				gbc_scrollPane_2.gridy = 8;
				contentPane.add(scrollPane_2, gbc_scrollPane_2);
				usersList = new JList();
				scrollPane_2.setViewportView(usersList);
				JScrollPane scrollPane_3 = new JScrollPane();
				GridBagConstraints gbc_scrollPane_3 = new GridBagConstraints();
				gbc_scrollPane_3.gridwidth = 3;
				gbc_scrollPane_3.fill = GridBagConstraints.BOTH;
				gbc_scrollPane_3.gridx = 1;
				gbc_scrollPane_3.gridy = 8;
				contentPane.add(scrollPane_3, gbc_scrollPane_3);
				detailsArea = new JTextArea();
				scrollPane_3.setViewportView(detailsArea);
				return contentPane;
			}

		private void createAndShowGUI(String title)
			{
				JFrame frame = new JFrame(title);
				frame.setContentPane(createPanel());
				frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
				frame.pack();
				frame.setVisible(true);
			}

		/**
		 * Launch the application.
		 */
		public ClientView(String title)
			{
				createAndShowGUI(title);
			}

		public void enableUserNameFiled(boolean enable)
			{
				userNameField.setEnabled(enable);
			}

		public void enableServerIPField(boolean enable)
			{
				serverIPField.setEnabled(enable);
			}

		public void updateUserList(Vector<String> userList)
			{
				usersList.setListData(userList);
			}

		public String getUserName()
			{
				return userNameField.getText();
			}

		public String getServerIP()
			{
				return serverIPField.getText();
			}

		public String getMessage()
			{
				return messageField.getText();
			}

		public void clearMessageLine()
			{
				messageField.setText("");
			}

		public void displayMessasge(String message, Color color)
			{
				messagesArea.setEditable(false);
				Color c = messagesArea.getForeground();
				messagesArea.setForeground(color);
				messagesArea.append(message);
				messagesArea.setForeground(c);
			}

		public void btnExitActionListener(ActionListener al)
			{
				btnExit.addActionListener(al);
			}

		public void btnLoginActionListener(ActionListener al)
			{
				btnLogin.addActionListener(al);
			}

		public void btnSendActionListener(ActionListener al)
			{
				btnSend.addActionListener(al);
			}

		public String getSelectedUserFromList()
			{
				return (String) usersList.getSelectedValue();
			}
	}
