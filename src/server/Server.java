package server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import client.ClientInterface;

/**
 * @author Cristian Onciu
 *
 */
public class Server extends UnicastRemoteObject
	{
		protected ArrayList<ClientInterface>	clients	= new ArrayList<ClientInterface>();

		protected Server() throws RemoteException
			{
				super();
			}
	}
