package server;

import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import javax.naming.NamingException;
import client.ClientInterface;

/**
 * @author Cristian Onciu
 *
 */
public class ServerTest
	{
		/**
		 * @param args
		 * @throws NamingException
		 * @throws UnknownHostException
		 * @throws RemoteException
		 * @throws MalformedURLException
		 */
		public static void main(String[] args) throws RemoteException,
				UnknownHostException, NamingException, MalformedURLException
			{
				ServerFrame view = new ServerFrame();
				ServerController sc = new ServerController(view);
			}
	}