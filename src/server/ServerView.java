package server;

/**
 * @author Cristian Onciu
 *
 */
public interface ServerView
	{
		public void displayText(String text);
	}
