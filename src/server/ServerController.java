package server;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Vector;
import javax.naming.NamingException;
import client.ClientInterface;

/**
 * @author Cristian Onciu
 *
 */
public class ServerController implements ServerInterface, Serializable
	{
		private static final long							serialVersionUID	= 1L;
		private ServerView										view;
		// private Server server;
		protected ArrayList<ClientInterface>	clients						= new ArrayList<ClientInterface>();
		private Map<ClientInterface, String>	map								= new HashMap<ClientInterface, String>();

		// private ClientController cc;
		// private UserImpl users;
		// private Vector<RemoteObserver> obs = new Vector<RemoteObserver>();
		// private Vector<ServerView> obs;
		// private ServerFrame frame;
		public ServerController(ServerView view) throws RemoteException,
				UnknownHostException, NamingException, MalformedURLException
			{
				super();
				this.view = view;
				view.displayText("Constructing server...");
				// server = new Server();
				view.displayText("Binding server to registry");
				Registry srv = LocateRegistry.createRegistry(1099);
				ServerInterface server = (ServerInterface) UnicastRemoteObject
						.exportObject(this, 1099);
				srv.rebind("friendship", server);
				// UnicastRemoteObject.exportObject(this, 0);
				view.displayText("Server binded on " + InetAddress.getLocalHost());
				view.displayText("Waiting for invocations from clients...");
			}

		@Override
		public void login(ClientInterface client, String userName)
				throws RemoteException
			{
				clients.add(client);
				map.put(client, userName);
				view.displayText(userName + " logged in.");
				update();
			}

		@Override
		public void sendMessage(String message, String fromUser, String toUser)
				throws RemoteException
			{
				for (int i = 0; i < clients.size(); i++)
					{
						ClientInterface c = clients.get(i);
						try
							{
								if (map.get(c).equalsIgnoreCase(toUser))
									{
										c.getMessage(message, fromUser);
										view.displayText("Message from " + fromUser + " to "
												+ toUser + ":\n\t" + message + "\n\tsent OK!");
									}
							} catch (RemoteException e)
							{
								/**
								 * If a client is not accessible, then it is removed from the
								 * ArrayList and the index i is decremented because all other
								 * clients go down one place
								 */
								logout(c);
								i = i - 1;
							}
					}
			}

		@Override
		public void logout(ClientInterface client) throws RemoteException
			{
				view.displayText(map.get(client) + " logged out.");
				clients.remove(client);
				update();
			}

		@Override
		public Vector<String> getUserList() throws RemoteException
			{
				Vector<String> userNameList = new Vector<String>();
				for (ClientInterface c : clients)
					userNameList.add(map.get(c));
				return userNameList;
			}

		@Override
		public Vector<String> getRecommendations(String fromUser)
				throws RemoteException
			{
				Vector<String> rec = new Vector<String>();
				for (ClientInterface c : clients)
					if (map.get(c).equals(fromUser))
						rec.addAll(c.getReccomendations());
				return rec;
			}

		public void update() throws RemoteException
			{
				for (ClientInterface c : clients)
					c.updateUserList();
			}

	}
