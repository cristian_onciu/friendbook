package server;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Vector;
import client.ClientInterface;

/**
 * @author Cristian Onciu
 *
 */
public interface ServerInterface extends Remote
	{
		public void login(ClientInterface client, String userName)
				throws RemoteException;

		public void sendMessage(String message, String fromUser, String toUser)
				throws RemoteException;

		public Vector<String> getUserList() throws RemoteException;

		public Vector<String> getRecommendations(String fromUser)
				throws RemoteException;

		public void logout(ClientInterface client) throws RemoteException;
	}
