package model;

import java.io.Serializable;
import java.util.Vector;

/**
 * @author Cristian Onciu
 *
 */
public class User implements Serializable
	{
		private String				name;
		private Vector<String>	friends;

		public User(String name)
			{
				this.name = name;
				friends = new Vector<String>();
			}

		public String getUserName()
			{
				return name;
			}

		public void addFriend(String userName)
			{
				friends.add(userName);
			}

		public void removeFriend(String friend)
			{
				friends.remove(friend);
			}

		public Vector<String> recommendFriends()
			{
				return friends;
			}

		@Override
		public String toString()
			{
				StringBuilder builder = new StringBuilder();
				builder.append("User [name=");
				builder.append(name);
				builder.append(", friends=");
				builder.append(friends);
				builder.append("]");
				return builder.toString();
			}
		
	}
